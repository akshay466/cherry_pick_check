﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EmployeeMicroservices.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        List<Employee> employees = new List<Employee>();
        // GET: api/<EmployeeController>
        [HttpGet]
        public IEnumerable<Employee> Get()
        {
            var employee1 = new Employee
            {
                Id = 1,
                Name = "John",
                Gender = "Male",
                Salary = 25000.36
            };

            var employee2 = new Employee
            {
                Id = 2,
                Name = "James",
                Gender = "Male",
                Salary = 27000.98
            };

            var employee3 = new Employee
            {
                Id = 3,
                Name = "Janani",
                Gender = "Female",
                Salary = 29000.98
            };
            
            var employee4 = new Employee
            {
                Id = 4,
                Name = "Akshay",
                Gender = "Male",
                Salary = 30000.98
            };

            employees.Add(employee1);
            employees.Add(employee2);
            employees.Add(employee3);
            employees.Add(employee4);
            

            return employees;
        }

        // GET api/<EmployeeController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            //var customer = employees.Where(c => c.Id == id).FirstOrDefault();
            //return customer.ToString();
            // var employee = employees.ElementAt(id);
            //return employee.ToString();
            return "value";
        }

        // POST api/<EmployeeController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<EmployeeController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EmployeeController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
